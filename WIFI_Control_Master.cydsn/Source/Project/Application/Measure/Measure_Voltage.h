/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef _MEASURE_VOLTAGE_H_
#define _MEASURE_VOLTAGE_H_

#ifdef __cplusplus
extern "C"
{
#endif   

#include "BaseTypes.h"

#define VOLTAGE_DEFAULT_LOWER_LIMIT        0u    //0V
#define VOLTAGE_DEFAULT_UPPER_LIMIT    24000u    //24V


uint32_t Measure_Voltage_CalculateVoltageFromPercent(uint8_t ucPercentValue, uint8_t ucOutputIdx, bool bUseDefault);
uint16_t Measure_Voltage_CalculateAdcValue(uint32_t ulVoltage);
uint32_t Measure_Voltage_CalculateVoltageValue(uint16_t uiAdcValue);
void Measure_Voltage_SetNewLimits(uint32_t ulMinLimit, uint32_t ulMaxLimit, uint8_t ucOutputIdx);
uint32_t Measure_Voltage_GetSystemVoltage(void);
uint32_t Measure_Voltage_CalculateSystemVoltage(uint16_t uiAdcValue);
void Measure_Voltage_SetSystemVoltage(uint32_t ulSystemVoltage);

#ifdef __cplusplus
}
#endif    

#endif //_MEASURE_VOLTAGE_H_
