/* ========================================
 *
 * Copyright Eduard Kraemer, 2019
 *
 * ========================================
*/

#include "Measure_Temperature.h"
#include "Aom.h"
#include "stdint.h"
#include "stdbool.h"

/****************************************** Defines ******************************************************/
#define SERIES_RESISTOR_ false
#define SUPPLY_3_3V true        //Set true when the NTC supply voltage is 3.3V
#define SUPPLY_12V  false       //Set true when the NTC supply voltage is 12V

/****************************************** Variables ****************************************************/
typedef struct
{
    int16_t siTemperature;
    uint16_t uiAdcValue;
}tsThermoTable;

/* Look-up-table for the NTC value 
   Precalculated values with a 10k-Ohm series resistor. The whole calculation
   and NTC-value table is found in 
   "file://wdmycloud/e_kraemer/Elektrotechnik/Software-Entwicklung/SVN-Projects/LED_Wifi_Documentation/trunk/Sonstiges/NTC"
    Note - Temperature is given with additional decimal (-50 = -5.0°C)
*/

#if SERIES_RESISTOR_
static const tsThermoTable sThermoTableNTC[] =
{
    {-50	, 1765	},
    {0	    , 1448	},
    {50	    , 1180	},
    {100	, 958	},
    {150	, 777	},
    {200	, 630	},
    {250	, 511	},
    {300	, 416	},
    {350	, 340	},
    {400	, 279	},
    {450	, 229	},
    {500	, 190	},
    {550	, 158	},
    {600	, 132	},
    {650	, 110	},
    {700	, 93	},
    {750	, 78	},
    {800	, 67	},
    {850	, 57	}
};
#elif SUPPLY_12V
    //Values are based on 12V Supply voltage
static const tsThermoTable sThermoTableNTC[] =
{
    {-50	,716},
    {0		,571},
    {50		,458},
    {100	,370},
    {150	,300},
    {200	,246},
    {250	,202},
    {300	,167},
    {350	,139},
    {400	,117},
    {450	,98	},
    {500	,83	},
    {550	,71	},
    {600	,60	},
    {650	,52	},
    {700	,45	},
    {750	,39	},
    {800	,33	},
    {850	,29	}
};
#elif SUPPLY_3_3V
        //Values are based on 12V Supply voltage
static const tsThermoTable sThermoTableNTC[] =
{
    {-50	,197},
    {0		,157},
    {50		,126},
    {100	,101},
    {150	,82},
    {200	,67},
    {250	,55},
    {300	,46},
    {350	,38},
    {400	,32},
    {450	,27	},
    {500	,22	},
    {550	,19	},
    {600	,16	},
    {650	,14	},
    {700	,12	},
    {750	,10	},
    {800	,9	},
    {850	,8	}
};
#endif
/****************************************** Function prototypes ******************************************/
/****************************************** loacl functiones *********************************************/
/****************************************** External visible functiones **********************************/

//********************************************************************************
/*!
\author     Kraemer E.
\date       27.09.2019

\fn         Measure_Temperature_CalculateTemperature()

\brief      Calculate temperature value from the ADC value

\return     (uint16_t)ulResult - Returns the calculated temperature value.

\param      uiNTCAdcValue - Measured ADC value from the NTC-Circuit
***********************************************************************************/
uint16_t Measure_Temperature_CalculateTemperature (uint16_t uiNTCAdcValue)
{
    uint32_t ulResult;
    uint16_t uiDelta;
    uint16_t uiIdx;
    uint16_t uiIdxFirst = _countof(sThermoTableNTC) - 1;
    uint16_t uiIdxSecond = 0;

    // check for lower temperature limit
    if(uiNTCAdcValue <= sThermoTableNTC[uiIdxFirst].uiAdcValue)
        return sThermoTableNTC[uiIdxFirst].siTemperature;

    // check for upper temperature limit
    if (uiNTCAdcValue >= sThermoTableNTC[uiIdxSecond].uiAdcValue)
        return sThermoTableNTC[uiIdxSecond].siTemperature;

    // binary search
    while((uiIdxFirst - uiIdxSecond) > 1)
    {
        /* Get the middle index between both */
        uiIdx = (uiIdxSecond + uiIdxFirst) / 2;

        // if value is in the second half
        if(uiNTCAdcValue >= sThermoTableNTC[uiIdx].uiAdcValue)
        {
            uiIdxFirst = uiIdx;
        }
        else
        {
            uiIdxSecond = uiIdx;
        }
    }

    /*
        Equation for the linear interpolation:
        X = ((Y1 - Y) * (X2-X1)/(Y2-Y1)) + X1
    */
    uiDelta = sThermoTableNTC[uiIdxSecond].uiAdcValue - sThermoTableNTC[uiIdxFirst].uiAdcValue;

    if(uiDelta)
    {
        ulResult = sThermoTableNTC[uiIdxSecond].uiAdcValue - uiNTCAdcValue;
        ulResult *= sThermoTableNTC[uiIdxFirst].siTemperature - sThermoTableNTC[uiIdxSecond].siTemperature;
        ulResult /= uiDelta;
        ulResult += sThermoTableNTC[uiIdxSecond].siTemperature;
    }
    else
    {
        ulResult = (sThermoTableNTC[uiIdxFirst].siTemperature - sThermoTableNTC[uiIdxSecond].siTemperature) / 2;
    }

    return (uint16_t)ulResult;
}
