/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef _MEASURE_CURRENT_H_
#define _MEASURE_CURRENT_H_

#ifdef __cplusplus
extern "C"
{
#endif   


#include "BaseTypes.h"

uint16_t Measure_Current_CalculateCurrentValue(uint16_t uiAdcValue);
uint16_t Measure_Current_CalculateAdcValue(uint16_t uiMilliCurrent);


#ifdef __cplusplus
}
#endif    

#endif //_MEASURE_CURRENT_H_
