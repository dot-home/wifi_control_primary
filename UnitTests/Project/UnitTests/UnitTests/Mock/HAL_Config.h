#pragma once

#include "stdint.h"
#include "stdbool.h"

#define ADC_MAX_VAL 2047u
#define ADC_INPUT_DEFAULT_VREF_MV_VALUE 1200
#define DRIVE_OUTPUTS 2
