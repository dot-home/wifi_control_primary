#pragma once

#include "stdint.h"
#include "stdbool.h"

#define _countof(array) (sizeof(array) / sizeof(array[0]))