#include "pch.h"
#include "Measure_Current.h"


uint16_t Measure_Current_CalculateCurrentValue(uint16_t uiAdcValue);
uint16_t Measure_Current_CalculateAdcValue(uint16_t uiMilliCurrent);

#define SHUNT_RESISTOR      0.01        // in Ohm
#define ADC_REF_MILLIVOLT   1200   //mV
#define INA180_A2_GAIN      50      // Gain of the INA180 A2 derivate
//#define MAX_MEASURE_CURRENT ((ADC_REF_MILLIVOLT / INA180_A2_GAIN)/SHUNT_RESISTOR)   //in mA
#define MAX_MEASURE_CURRENT 2398    //Value in mA. Use this instead of calculation because of the rounding problems
#define MIN_MEASURE_CURRENT 0

#define ADC_MIN_VALUE   0u
#define ADC_MAX_VALUE   2047u

TEST(Test_Measure_Current, Test_CalculateCurrentValue_InvalidAdcValue)
{
    //Test invalid ADC values
    uint32_t ulCurrentVal = Measure_Current_CalculateCurrentValue(UINT16_MAX);
    
    //Expect the ADC value to be truncated to ADC-MAX-Value
    EXPECT_EQ(MAX_MEASURE_CURRENT, ulCurrentVal);

    //for (int AdcValue = 0; AdcValue <= 2047; AdcValue += 5)
    //{
    //    std::cout << "AdcValue:"<< AdcValue << " | Current Value:" << Measure_Current_CalculateCurrentValue(AdcValue) << std::endl;
    //} 
}


TEST(Test_Measure_Current, Test_CalculateCurrentValue_Limits)
{
    //Test invalid ADC values
    uint32_t ulCurrentVal = Measure_Current_CalculateCurrentValue(ADC_MIN_VALUE);

    //Expect the ADC value to be truncated to ADC-MAX-Value
    EXPECT_EQ(MIN_MEASURE_CURRENT, ulCurrentVal);

    //Test invalid ADC values
    ulCurrentVal = Measure_Current_CalculateCurrentValue(ADC_MAX_VALUE);

    //Expect the ADC value to be truncated to ADC-MAX-Value
    EXPECT_EQ(MAX_MEASURE_CURRENT, ulCurrentVal);
}

